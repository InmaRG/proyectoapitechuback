var https = require('https');
var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var moment = require('moment');
var XMLHttpRequest = require('w3c-xmlhttprequest').XMLHttpRequest;
/***********************Varibles para generar el token jwt******/
var jwt = require('jsonwebtoken');
var jwtClave="laclave_de_maestra jwt";
var port = process.env.port || 3000;
const bcrypt = require('bcrypt');
/**************************************************************/

var app = express();
app.use(bodyParser.json());

/************************Conexión con Mlab***********************************/

var baseMlabURL="https://api.mlab.com/api/1/databases/apitechuirg/collections/";
var mLabAPIKey="apiKey=ydHr1CAsisjtUK0BAObte44Xbh2NafEw";
var requestJSON = require('request-json');

/****************Ficheros con los certificados para realizar https ********/

var options = {
    key: fs.readFileSync('techu.key'),
    cert: fs.readFileSync('techu.crt')
};

/*****************Conexión con el servidor*******************************/

/*https.createServer(options, app).listen(3000, function(){
    console.log("El servidor esta escuchando en el puerto 3000");
    //console.log("Key: " + options.key);
    //console.log("Cert: " + options.cert);

});*/

app.listen(port);
console.log("API escuachado en el puerto:   " + port);

/**************************API*****************************************/

app.get('/apitechu/v1/', function (req, res) {
      res.send('Bienvenido a Apitechu con https');
});

/*************************busqueda de usuarios por id******************/

app.get('/apitechu/v1/users/',
     function (req, res) {
       console.log("GET /apitechu/v1/users/");
       var tokenjwt = req.headers.accesstoken;
       if(tokenjwt==undefined){
         response = {
           "msg" : "Se requiere un token jwt para realizar la petición"
         }
         res.status(401);
         res.send(response);

       }else {
             console.log("tokenjwt: " + tokenjwt);
             var token=validartoken(tokenjwt);
             var response;
             console.log("response: " +  JSON.stringify(token));
             if(token.id_user==-1) {
               response = {
                 "msg" : "Access-token no valido"
               }
               res.status(401);
               res.send(response);
             }else if(token.id_user==-2) {
               response = {
                 "msg" : "Access-token caducado"
               }
               res.status(401);
               res.send(response);

             }else {
                    var query = 'q={"id" : ' + token.id_user +'}';
                    console.log("Query: " + query);
                    httpClient = requestJSON.createClient(baseMlabURL);
                    console.log("Busqueda de cliente");
                    httpClient.get("user?"+ query + "&"+ mLabAPIKey,
                         function(err, resMLab, body) {
                             if (err) {
                                response = {
                                  "msg" : "Error obteniendo usuario."
                                }
                               res.status(500);
                               res.send(response);
                              } else {
                                  if (body.length > 0) {
                                      response = body[0];
                                      res.send(response);
                                  } else {
                                      response = {
                                          "msg" : "Usuario no encontrado."
                                      };
                                  res.status(404);
                                  res.send(response);
                                }//else
                             }//else
                           } //function
                  );//cierre del get
             }//else
           }//else if(tokenjwt=='undefined'){
    }//function (req, res)
);



/******************Login*********************************/

app.post('/apitechu/v1/login',
     function (req, res) {
       console.log("Login Usuarios");

       console.log("req.body.email:" + req.body.email);
       console.log("req.body.pass:" + req.body.pass);
       var salt = bcrypt.genSaltSync(10);
       var pass=req.body.pass;
       console.log("req.body.pass:" + req.body.pass);
       var hash = bcrypt.hashSync(pass,10);
       console.log("hash: " + hash);
       //var response;
       httpClient = requestJSON.createClient(baseMlabURL);
       var query = 'q={"email":"'+req.body.email +'","password" :"' + hash+ '"}';
       console.log("query: " + query);
       httpClient.get("user?"+ query + "&"+ mLabAPIKey,
       function(err, resMLab, body) {
         if (err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
           res.status(500);
           res.send(response);
          } else {
              if (body.length > 0) {
                var payload = {
                   "id_user" :  body[0].id,
                   "exp" : moment().add(14,"days").unix(),
                }
                console.log("Payload: " + JSON.stringify(payload));

                response = {
                  jwt : jwt.sign(payload,jwtClave)
                }
                res.send(response);

              } else {
                  response = {
                      "msg" : "Usuario no encontrado."
                  };
              res.status(404);
              res.send(response);
            }//else
         }//else

       } //function
      );//cierre del get
   }//function (req, res)
);

/***********************Obtener cuentas de un cliente*****************/


app.get('/apitechu/v1/users/accounts',
     function (req, res) {

       console.log("GET /apitechu/v1/users/accounts");
       var tokenjwt = req.headers.accesstoken;
       var response;

       if(tokenjwt==undefined){
         response = {
           "msg" : "Se requiere un token jwt para realizar la petición"
         }
         res.status(401);
         res.send(response);

       }else {

           var token=validartoken(tokenjwt);
           var query = 'q={"id_user":'+ token.id_user +'}';
           console.log("Query : " + query);
           if(token.id_user==-1) {
             response = {
               "msg" : "Access-token no valido"
             }
             res.status(401);
             res.send(response);
           }else if(token.id_user==-2) {
             response = {
               "msg" : "Access-token caducado"
             }
             res.status(401);
             res.send(response);

           }else {
                 httpClient = requestJSON.createClient(baseMlabURL);
                 console.log("Consulta de Cuentas");
                 httpClient.get("account?"+ query + "&"+ mLabAPIKey,
                     function(err, resMLab, body) {
                     if (err) {
                         response = {
                          "msg" : "Error obteniendo cuentas."
                      }
                     res.status(500);
                     res.send(response);
                     } else {
                       if (body.length > 0) {
                            console.log("El usuario tiene cuentas");
                            response = !err ? body : {
                                "msg" : "Error obteniendo cuentas"
                            }
                            res.send(response);

                        } else {
                            response = {
                                "msg" : "El usuario no tiene cuentas"
                            };
                        res.status(404);
                        res.send(response);
                      }//else if (body.length > 0)*/
                   }//else if(err)
                  }// function (err, resMLab, body)*/
                );//httpClient.get("user?"+ query + "&"+ mLabAPIKey,
           }
         }//if(tokenjwt==undefined){
   }//function (req, res)
);


app.post('/apitechu/v1/user',
     function (req, res) {
       console.log("GET /apitechu/v1/user");
       var datauser = req.body.datauser;
       console.log("datauser: "+ JSON.stringify(datauser));
       var response;
       var query='s={"id":-1}&l=1';
       var newuser;


       console.log("newuser: " + JSON.stringify(newuser));
       console.log("query: " + query);
       httpClient = requestJSON.createClient(baseMlabURL);
       httpClient.get("user?"+ query + "&"+ mLabAPIKey,
           function(err, resMLab, body) {
           if (err) {
               response = {
                "msg" : "Error obteniendo datos de la BB.DD."
            }
           res.status(500);
           res.send(response);
           } else {
             if (body.length > 0) {
                  console.log("Hay resgistros en la BB.DD");
                  console.log("id: " + body[0].id);
                  var id_newuser=body[0].id+1;
                  var salt = bcrypt.genSaltSync(10);
                  var pass=req.body.datauser.password;

                  var hash = bcrypt.hashSync(pass,salt);
                  console.log("pass: " + pass);
                  console.log("hash: " + hash);
                  console.log("req.body.datauser.nombre: " + req.body.datauser.nombre);


                  if(bcrypt.compareSync(req.body.datauser.password, hash)) {
                    console.log("password correcta");
                    } else {
                      console.log("passwor incorrecta");
                     }
                  newuser = {
                     id: id_newuser,
                     nombre:req.body.datauser.nombre,
                     apellidos:req.body.datauser.apellidos,
                     email:req.body.datauser.email,
                     password : hash,
                     direccion : req.body.datauser.direccion,
                     ciudad : req.body.datauser.ciudad,
                     provincia : req.body.datauser.provincia,
                     pais : req.body.datauser.pais
                   };
                   console.log("newuser: " + JSON.stringify(newuser));
                   httpClient.post("user?&"+mLabAPIKey,newuser,
                         function(errPost,resMLabPost,bodyPost){
                           console.log("Llega al post");
                              /*response = tokenjwt;
                              res.send(response);*/
                         }//funcion(errPost,resMLabPost,bodyPost)
                    )//httpClient.post
              } else {
                  response = {
                      "msg" : "El usuario no tiene cuentas"
                  };
              res.status(404);
              res.send(response);
            }//else if (body.length > 0)*/
         }//else if(err)
        }// function (err, resMLab, body)*/
      );//httpClient.get("user?"+ que

    }
);

function obtenerdataBBDD(tabla,query) {
  var peticion = new XMLHttpRequest();
  var url = baseMlabURL+tabla+"?"+query+"&"+mLabAPIKey;
  console.log("url: " + url);
  peticion.open("GET", url, false);
  peticion.setRequestHeader("Content-Type", "application/json");

  peticion.send();
  console.log("peticion.responseText: " + peticion.responseText);
  var valores = JSON.parse(peticion.responseText);

  console.log("valores: " + valores);

  var estado;


/*  httpClient = requestJSON.createClient(baseMlabURL);
  var response1=httpClient.get(tabla+"?"+ query + "&"+ mLabAPIKey,
      function(err, res, body) {
          console.log("Funcion get http");
          console.log("variable res: " + JSON.stringify(res));
          console.log("variable err: " + JSON.stringify(err));
          console.log("variable body: " + JSON.stringify(body));

           return "ok";
          /*if (err) {
              res.status(500);
              response = {
                "mensaje" : "Error de conexión a la BB.DD"
              };
              res.send(response);
          } else {
               if (body.length > 0) {
                  /*var payload = {
                     "id_user" :  body[0].id,
                    "exp" : moment().add(14,"days").unix(),
                  }
                  console.log("Payload: " + JSON.stringify(payload));*/

                   /*httpClient.post("tokenjwt?&"+mLabAPIKey,tokenjwt,
                        function(errPost,resMLabPost,bodyPost){
                          console.log("Llega al post");
                             response = tokenjwt;
                             res.send(response);
                        }//funcion(errPost,resMLabPost,bodyPost)
                   )//httpClient.post(*/

                /*  var tokenjwt = jwt.sign(payload,jwtClave);
                  estado=200;
                  console.log("estado: " + estado);
                  res = {
                      "mensaje" : tokenjwt
                  }
                  console.log("tokenjwt: " + tokenjwt);
                  console.log("res: " + JSON.stringify(res));
                  console.log("res estatus: " + res.status);
             /*var decjwt=jwt.decode(tokenjwt,jwtClave);
             console.log("decode jwt: " + JSON.stringify(decjwt));*/

            /*} else {

                res.status=404;
                response = {
                   "mensaje" : "Información no encontrada"
                }
                res.send(response);
         }//else if (body.length > 0)
    }//else if(err)*/

  /* }// function (err, body)*/
 /*);//httpClient.get("user?"+ query + "&"+ mLabAPIKey,
 //console.log("estado en la funcion: " + estado);
 console.log("response1111111111111: " + JSON.stringify(response1));*/
}//funcion obtener datos

function validartoken(tokenjwt){

  var payload=jwt.decode(tokenjwt,jwtClave);
  console.log("payload: " + payload);
  var respuesta;
  if(payload==null) {
     respuesta = {
       "id_user" : -1
     }
  } else {
    if(payload.exp <= moment().unix()) {
      respuesta = {
        "id_user" : -2,
      }
    }else {
        respuesta=payload;
      }

    }
    return(respuesta);
}
