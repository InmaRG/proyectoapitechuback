var https = require('https');
var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var moment = require('moment');
var XMLHttpRequest = require('w3c-xmlhttprequest').XMLHttpRequest;
/***********************Varibles para generar el token jwt******/
var jwt = require('jsonwebtoken');
var jwtClave="laclave_de_maestra jwt";
var port = process.env.port || 3000;
const bcrypt = require('bcrypt');
/**************************************************************/

var app = express();
app.use(bodyParser.json());

/************************Conexión con Mlab***********************************/

var baseMlabURL="https://api.mlab.com/api/1/databases/apitechuirg/collections/";
var mLabAPIKey="apiKey=ydHr1CAsisjtUK0BAObte44Xbh2NafEw";
var requestJSON = require('request-json');

/****************Ficheros con los certificados para realizar https ********/

var options = {
    key: fs.readFileSync('techu.key'),
    cert: fs.readFileSync('techu.crt')
};

/*****************Conexión con el servidor*******************************/

/*https.createServer(options, app).listen(3000, function(){
    console.log("El servidor esta escuchando en el puerto 3000");
});*/

app.listen(port);
console.log("API escuachado en el puerto:   " + port);

/**************************API*****************************************/

app.get('/apitechu/v1/', function (req, res) {
      res.send('Bienvenido a Apitechu con https');
});

/*************************busqueda de usuarios por id******************/

app.get('/apitechu/v1/users',
     function (req, res) {
       console.log("GET /apitechu/v1/users/");
       var tokenjwt = req.headers.accesstoken;
       if(tokenjwt==undefined){
         response = {
           "msg" : "Se requiere un token jwt para realizar la petición"
         }
         res.status(401);
         res.send(response);

       }else {
             console.log("tokenjwt: " + tokenjwt);
             var token=validartoken(tokenjwt);
             var response;
             console.log("response: " +  JSON.stringify(token));
             if(token.email==-1) {
               response = {
                 "msg" : "Access-token no valido"
               }
               res.status(401);
               res.send(response);
             }else if(token.email==-2) {
               response = {
                 "msg" : "Access-token caducado"
               }
               res.status(401);
               res.send(response);

             }else {
                    var query = 'q={"email" : "' + token.email +'"}';
                    console.log("Query: " + query);
                    httpClient = requestJSON.createClient(baseMlabURL);
                    console.log("Busqueda de cliente");
                    httpClient.get("user?"+ query + "&"+ mLabAPIKey,
                         function(err, resMLab, body) {
                             if (err) {
                                response = {
                                  "msg" : "Error obteniendo usuario."
                                }
                               res.status(500);
                               res.send(response);
                              } else {
                                  if (body.length > 0) {
                                      response = body[0];
                                      res.send(response);
                                  } else {
                                      response = {
                                          "msg" : "Usuario no encontrado."
                                      };
                                  res.status(404);
                                  res.send(response);
                                }//else
                             }//else
                           } //function
                  );//cierre del get
             }//else
           }//else if(tokenjwt=='undefined'){
    }//function (req, res)
);



/******************Login*********************************/

app.post('/apitechu/v1/login',
     function (req, res) {
       console.log("Login Usuarios");

       console.log("req.body.email:" + req.body.email);
       console.log("req.body.pass:" + req.body.pass);
       var salt = bcrypt.genSaltSync(10);
       var pass=req.body.pass;

       //var response;
       httpClient = requestJSON.createClient(baseMlabURL);
       var query = 'q={"email":"'+req.body.email +'"}';
       console.log("query: " + query);
       httpClient.get("user?"+ query + "&"+ mLabAPIKey,
       function(err, resMLab, body) {
         if (err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
           res.status(500);
           res.send(response);
          } else {
              if (body.length > 0) {
                  var hash=body[0].password;
                  if(bcrypt.compareSync(req.body.pass, hash)) {
                      console.log("password correcta");
                      var payload = {
                         "email" :  body[0].email,
                         "exp" : moment().add(14,"days").unix(),
                      }
                      console.log("Payload: " + JSON.stringify(payload));
                      response = {
                        jwt : jwt.sign(payload,jwtClave)
                      }
                      res.send(response);

                  } else {
                      console.log("password incorrecta");
                      response = {
                          "msg" : "Password Incorrecta"
                      };
                      res.status(409);
                      res.send(response);
                  }

              } else {
                  response = {
                      "msg" : "Usuario no encontrado."
                  };
                  res.status(404);
                  res.send(response);
            }//else
         }//else

       } //function
      );//cierre del get
   }//function (req, res)
);

/***********************Obtener cuentas de un cliente*****************/

app.get('/apitechu/v1/users/accounts',
     function (req, res) {

       console.log("GET /apitechu/v1/users/accounts");
       var tokenjwt = req.headers.accesstoken;

       console.log("req.headers: " +req.headers.accesstoken);

       console.log("tokenjwt: " +tokenjwt);

       var response;

       if(tokenjwt==undefined){
         response = {
           "msg" : "Se requiere un token jwt para realizar la petición"
         }
         res.status(401);
         res.send(response);

       }else {

           var token=validartoken(tokenjwt);
           var query = 'q={"email":"'+ token.email +'"}';
           console.log("Query : " + query);
           if(token.email==-1) {
             response = {
               "msg" : "Access-token no valido"
             }
             res.status(401);
             res.send(response);
           }else if(token.email==-2) {
             response = {
               "msg" : "Access-token caducado"
             }
             res.status(401);
             res.send(response);

           }else {
                 httpClient = requestJSON.createClient(baseMlabURL);
                 console.log("Consulta de Cuentas");
                 httpClient.get("accounts?"+ query + "&"+ mLabAPIKey,
                     function(err, resMLab, body) {
                     if (err) {
                         response = {
                          "msg" : "Error obteniendo cuentas."
                      }
                     res.status(500);
                     res.send(response);
                     } else {
                       if (body.length > 0) {
                            console.log("El usuario tiene cuentas");
                            response = !err ? body : {
                                "msg" : "Error obteniendo cuentas"
                            }
                            res.send(response);

                        } else {
                            response = {
                                "msg" : "El usuario no tiene cuentas"
                            };
                            res.send(response);
                      }//else if (body.length > 0)*/
                   }//else if(err)
                  }// function (err, resMLab, body)*/
                );//httpClient.get("user?"+ query + "&"+ mLabAPIKey,
           }
         }//if(tokenjwt==undefined){
   }//function (req, res)
);


/***********************Crear cuentas de un cliente*****************/

app.post('/apitechu/v1/users/accounts',
     function (req, res) {
        console.log("POST /apitechu/v1/users/accounts");
        var tokenjwt=req.headers.accesstoken;
        console.log("req.headers: " +req.headers.accesstoken);
        console.log("tokenjwt: " +tokenjwt);
        var response;
        if(tokenjwt==undefined){
         response = {
           "msg" : "Se requiere un token jwt para realizar la petición"
         }
         res.status(401);
         res.send(response);

       }else {
           var token=validartoken(tokenjwt);
           var query = 'q={"email":"'+ token.email +'"}';
           console.log("Query : " + query);
           if(token.email==-1) {
             response = {
               "msg" : "Access-token no valido"
             }
             res.status(401);
             res.send(response);
           }else if(token.email==-2) {
             response = {
               "msg" : "Access-token caducado"
             }
             res.status(401);
             res.send(response);

           }else {
               httpClient = requestJSON.createClient(baseMlabURL);
               var iban = {
                    "email" : token.email,
                    "iban": generarNumeroCuenta(),
                    "total" : 0
                }

                httpClient.post("accounts?&"+mLabAPIKey,iban,
                    function(errPost,resMLabPost,bodyPost){
                    
                        if (errPost) {
                            response = {
                                "msg" : "Error creando la cuenta."
                            }
                            res.status(500);
                            res.send(response);
                        }else {
                                console.log("Se da de alta la cuenta");
                                response = {
                                    "msg" : "Cuenta creada con existo"
                                }
                                res.send(response);
                        }
                     }
                    );
           }
       }//if(tokenjwt==undefined){
   }//function (req, res)
);

/****************************Alta de usuario*********************************/
app.post('/apitechu/v1/user',
     function (req, res) {
       console.log("GET /apitechu/v1/user");
       var datauser = req.body.datauser;
       console.log("datauser: "+ JSON.stringify(datauser));
       var response;
       var query='q={"email":"'+req.body.datauser.email +'"}';
       var newuser;

       console.log("query: " + query);
       httpClient = requestJSON.createClient(baseMlabURL);
       httpClient.get("user?"+ query + "&"+ mLabAPIKey,
           function(err, resMLab, body) {
           if (err) {
               response = {
                "msg" : "Error obteniendo datos de la BB.DD."
            }
           res.status(500);
           res.send(response);
           } else {
                if (body.length > 0) {
                    response = {
                        "msg" : "El email ya se encuentra registrado"
                    };
                    res.status(409);
                    res.send(response);
                } else {
                  console.log("El email no esta dado de alta");
                  var salt = bcrypt.genSaltSync(10);
                  var pass=req.body.datauser.password;
                  var hash = bcrypt.hashSync(pass,salt);
                  console.log("pass: " + pass);
                  console.log("hash: " + hash);
                  console.log("req.body.datauser.nombre: " + req.body.datauser.nombre);

                  newuser = {
                       nombre:req.body.datauser.nombre,
                       apellidos:req.body.datauser.apellidos,
                       email:req.body.datauser.email,
                       password : hash,
                       direccion : req.body.datauser.direccion,
                       ciudad : req.body.datauser.ciudad,
                       provincia : req.body.datauser.provincia,
                       pais : req.body.datauser.pais
                  };
                  console.log("datauser: " + JSON.stringify(req.body.datauser));
                  console.log("newuser: " + JSON.stringify(newuser));    
                  httpClient.post("user?&"+mLabAPIKey,newuser,
                     function(errPost,resMLabPost,bodyPost){
                       console.log("Llega al post");
                       if (errPost) {
                           response = {
                            "msg" : "Error obteniendo datos de la BB.DD."
                        }
                       res.status(500);
                       res.send(response);
                       } else {
                           var iban = {
                             "email" : req.body.datauser.email,
                             "iban": generarNumeroCuenta(),
                             "total" : 0
                           }

                           httpClient.post("accounts?&"+mLabAPIKey,iban,
                              function(errPost,resMLabPost,bodyPost){
                                console.log("Se da de alta la cuenta");
                              }
                            );

                           response = {
                            "msg" : "Usuario creado con exito"
                           }
                           res.send(response);
                       }

                     }//funcion(errPost,resMLabPost,bodyPost)
                )//httpClient.post
            }//else if (body.length > 0)*/
         }//else if(err)
        }// function (err, resMLab, body)*/
      );//httpClient.get("user?"+ que
    }
);

/****************************Realizar Operacion*******************************/

app.post('/apitechu/v1/users/accounts/transactions',
     function (req, res) {

       console.log("POST /apitechu/v1/users/accounts/transactions");
       var tokenjwt = req.headers.accesstoken;
       console.log("tokenjwt: " +tokenjwt);
       console.log("req.body.iban:" + req.body.operacion.iban);
       console.log("req.body.tipo_operacion:" + req.body.operacion.tipo_operacion);
       console.log("req.body.importe:" + req.body.operacion.importe);
       var fecha=moment().format('DD/MM/YYYY, HH:mm:ss');
       console.log("Fecha: " + fecha);
       req.body.operacion.fecha=fecha;
       console.log("Fecha req: " + req.body.operacion.fecha);

       var response;

       if(tokenjwt==undefined){
         response = {
           "msg" : "Se requiere un token jwt para realizar la petición"
         }
         res.status(401);
         res.send(response);

       }else {

           var token=validartoken(tokenjwt);
           var query = 'q={"email":"'+ token.email +'","iban":"'+req.body.operacion.iban+'"}';
           console.log("Query : " + query);
           if(token.email==-1) {
             response = {
               "msg" : "Access-token no valido"
             }
             res.status(401);
             res.send(response);
           }else if(token.email==-2) {
             response = {
               "msg" : "Access-token caducado"
             }
             res.status(401);
             res.send(response);

           }else {
                console.log("token valido");
                httpClient = requestJSON.createClient(baseMlabURL);
                  httpClient.get("accounts?"+ query + "&"+ mLabAPIKey,
                    function(err, resMLab, body) {
                            if (err) {
                               response = {
                                "msg" : "Error obteniendo datos de la BB.DD."
                               }
                               res.status(500);
                               res.send(response);
                            } else {
                                if (body.length > 0) {
                                    console.log("La cuenta pertenece al cliente");
                                    var saldo;
                                    var putBody;
                                    if(req.body.operacion.tipo_operacion=="1"){
                                        
                                        saldo=parseInt(body[0].total)+parseInt(req.body.operacion.importe);
                                        req.body.operacion.saldo=saldo;
                                        console.log("Saldo: " + saldo);
                                        putBody = '{"$set":{"total":"'+saldo+'"}}';
                                        console.log("putBody: " + putBody);
                                        httpClient.put("accounts?"+query+"&"+mLabAPIKey,JSON.parse(putBody),
                                                function(errPut,resMLabPut,bodyPut){
                                                    console.log("Llega al put");
                                                    console.log("errPut:" + errPut);
                                                    if (errPut) {
                                                        response = {
                                                            "msg" : "Error obteniendo datos de la BB.DD."
                                                        }
                                                        res.status(500);
                                                        res.send(response);
                                                    } else {
                                                        httpClient.post("transactions?&"+mLabAPIKey,req.body.operacion);
                                                        response = {
                                                            "msg" : "Transaccion realizada con exito"
                                                        }
                                                        res.send(response);
                                                    }
                                                }//funcion(errPut,resMLabPut,bodyPut)             
                                            );
                                     }else{
                                         saldo=parseInt(body[0].total)-parseInt(req.body.operacion.importe);
                                         if(saldo<0) {
                                             response = {
                                                    "msg" : "No dispone de saldo"
                                             }
                                             res.status(409);
                                             res.send(response);
                                         }else{
                                                putBody = '{"$set":{"total":"'+saldo+'"}}';
                                                console.log("putBody: " + putBody);
                                                req.body.operacion.saldo=saldo;
                                                req.body.operacion.importe="-"+req.body.operacion.importe;
                                                httpClient.put("accounts?"+query+"&"+mLabAPIKey,JSON.parse(putBody),
                                                    function(errPut,resMLabPut,bodyPut){
                                                        console.log("Llega al put");
                                                        console.log("errPut:" + errPut);
                                                        if (errPut) {
                                                            response = {
                                                                "msg" : "Error obteniendo datos de la BB.DD."
                                                            }
                                                            res.status(500);
                                                            res.send(response);
                                                        } else {
                                                            httpClient.post("transactions?&"+mLabAPIKey,req.body.operacion);
                                                            response = {
                                                                "msg" : "Transaccion realizada con exito"
                                                            }
                                                            res.send(response);
                                                        }
                                                    }//funcion(errPut,resMLabPut,bodyPut)             
                                            );   
                                         }
                                         
                                     }
                                    
                                    
                                }
                                else{
                                    response = {
                                       "msg" : "Acceso No Autorizado"
                                    }
                                    res.status(401);
                                    res.send(response);
                                }
                            }
                    }
                  );//httpClient.get
           }
         }//if(tokenjwt==undefined){
   }//function (req, res)
);

/**************************************************************************************************************************************/

app.get('/apitechu/v1/users/accounts/transactions',
     function (req, res) {

       console.log("GET /apitechu/v1/users/accounts/transactions");
       var tokenjwt = req.headers.accesstoken;
       var iban = req.headers.iban;
       console.log("tokenjwt: " +tokenjwt);
       console.log("req.body.iban:" + req.headers.iban);
       
       var response;

       if(tokenjwt==undefined){
         response = {
           "msg" : "Se requiere un token jwt para realizar la petición"
         }
         res.status(401);
         res.send(response);

       }else {

           var token=validartoken(tokenjwt);
           var query = 'q={"email":"'+ token.email +'"}';
           console.log("Query : " + query);
           if(token.email==-1) {
             response = {
               "msg" : "Access-token no valido"
             }
             res.status(401);
             res.send(response);
           }else if(token.email==-2) {
             response = {
               "msg" : "Access-token caducado"
             }
             res.status(401);
             res.send(response);

           }else {
                console.log("token valido");
                var query = 'q={"email":"'+ token.email +'","iban":"'+req.headers.iban+'"}';
               
                console.log("Query : " + query);
                httpClient = requestJSON.createClient(baseMlabURL);
                httpClient.get("accounts?"+ query + "&"+ mLabAPIKey,
                    function(err, resMLab, body) {
                        if (err) {
                            response = {
                            "msg" : "Error obteniendo datos de la BB.DD."
                            }
                            res.status(500);
                            res.send(response);
                        } else {
                            if (body.length > 0) {
                                console.log("La cuenta pertenece al cliente");
                                var query1='q={"iban":"'+req.headers.iban+'"}&s={"fecha":-1}';
                                console.log("query1: " + query1);
                                httpClient.get("transactions?"+ query1 + "&"+ mLabAPIKey,
                                    function(err1, resMLab1, body1) {
                                        if (err1) {
                                            response = {
                                                "msg" : "Error obteniendo datos de la BB.DD."
                                            }
                                            res.status(500);
                                            res.send(response);
                                        } else {
                                                if (body1.length > 0) {
                                                    console.log("El usuario tiene movimientos");
                                                    response = !err1 ? body1 : {
                                                        "msg" : "Error obteniendo movimientos"
                                                    }
                                                    res.send(response);
                                                }
                                                else {
                                                    response = {
                                                        "msg" : "No dispone de movimientos"
                                                    }
                                                    res.send(response);
                                                }
                                        }
                                    }
                                );        
                            }//if (body.length > 0) {
                             else{
                                response = {
                                   "msg" : "Acceso No Autorizado"
                                }
                                res.status(401);
                                res.send(response);
                            }
                        }
                    }
                );
                  
               
           }
         }//else if(tokenjwt==undefined){
   }//function (req, res)
);

/****************************Realizar Transferencia*******************************/

app.post('/apitechu/v1/users/accounts/transferencia',
     function (req, res) {
        console.log("GET /apitechu/v1/users/accounts/transferencia");
        var tokenjwt = req.headers.accesstoken;
        console.log("tokenjwt: " +tokenjwt);
        console.log("req.body.transferencia.iban_origen:" + req.body.transferencia.iban_origen);
    
        var response;

        if(tokenjwt==undefined){
             response = {
               "msg" : "Se requiere un token jwt para realizar la petición"
             }
             res.status(401);
             res.send(response);
       }else {
               var token=validartoken(tokenjwt);
               var query = 'q={"email":"'+ token.email +'"}';
               console.log("Query : " + query);
               if(token.email==-1) {
                 response = {
                   "msg" : "Access-token no valido"
                 }
                 res.status(401);
                 res.send(response);
               }else if(token.email==-2) {
                 response = {
                   "msg" : "Access-token caducado"
                 }
                 res.status(401);
                 res.send(response);

               }else {
                    console.log("token valido");
                    var saldo_origen;
                    var query = 'q={"email":"'+ token.email +'","iban":"'+req.body.transferencia.iban_origen+'"}';
                    console.log("Query : " + query);
                    httpClient = requestJSON.createClient(baseMlabURL);
                    httpClient.get("accounts?"+ query + "&"+ mLabAPIKey,
                        function(err, resMLab, body) {
                            if (err) {
                                response = {
                                "msg" : "Error obteniendo datos de la BB.DD."
                                }
                                res.status(500);
                                res.send(response);
                            } else {
                                if (body.length > 0) {
                                    console.log("La cuenta pertenece al cliente");
                                    saldo_origen=parseInt(body[0].total)-parseInt(req.body.transferencia.importe);
                                    if(saldo_origen<0){
                                        response = {
                                            "msg" : "No dispone de saldo"
                                        }
                                        res.status(409);
                                        res.send(response);
                                        
                                    }else{
                                        var query_destino = 'q={"iban":"'+req.body.transferencia.iban_destino+'"}';
                                        console.log("query_destino: " + query_destino);
                                        httpClient.get("accounts?"+ query_destino + "&"+ mLabAPIKey,
                                            function(errDestino, resMLabDestino, bodyDestino) {
                                                if (errDestino) {
                                                    response = {
                                                    "msg" : "Error obteniendo datos de la BB.DD."
                                                    }
                                                    res.status(500);
                                                    res.send(response);
                                                } else {
                                                    if (bodyDestino.length > 0) {
                                                        console.log("La cuenta existe");
                                                        console.log("Cuenta_destino: "+JSON.stringify(bodyDestino[0]));
                                                        fecha_operacion = moment().format('DD/MM/YYYY, HH:mm:ss');
                                                        console.log("saldo_destino: " +bodyDestino[0].total);
                                                        console.log("Importe de la transferencia: " +req.body.transferencia.importe);
                                                        var saldo_destino=parseInt(bodyDestino[0].total)+parseInt(req.body.transferencia.importe);
                                                        var dataorigen={
                                                            fecha: fecha_operacion,
                                                            iban:req.body.transferencia.iban_origen,
                                                            descripcion:"Transferencia realizada",
                                                            tipo_operacion:"-1",
                                                            importe: "-"+req.body.transferencia.importe,
                                                            ubicacion:"MonfortedeLemos,Madrid",
                                                            saldo: saldo_origen,
                                                            concepto:req.body.transferencia.concepto
                                                        }
                                                        console.log("dataorigen: "+JSON.stringify(dataorigen));
                                                        
                                                        var datadestino={
                                                            fecha: fecha_operacion,
                                                            iban:req.body.transferencia.iban_destino,
                                                            descripcion:"Transferencia recibida",
                                                            tipo_operacion:"1",
                                                            importe: req.body.transferencia.importe,
                                                            ubicacion:"MonfortedeLemos,Madrid",
                                                            saldo: saldo_destino,
                                                            concepto:req.body.transferencia.concepto
                                                        }
                                                        console.log("datadestino: "+JSON.stringify(datadestino));
                                                        var putBodyOrigen = '{"$set":{"total":"'+saldo_origen+'"}}';
                                                        var putBodyDestino = '{"$set":{"total":"'+saldo_destino+'"}}';
                                                        httpClient.put("accounts?"+query+"&"+mLabAPIKey,JSON.parse(putBodyOrigen));
                                                        httpClient.put("accounts?"+query_destino+"&"+mLabAPIKey,JSON.parse(putBodyDestino));
                                                        httpClient.post("transactions?&"+mLabAPIKey,dataorigen);
                                                        httpClient.post("transactions?&"+mLabAPIKey,datadestino);
                                                        
                                                        response = {
                                                            "msg" : "Transaccion realizada con exito"
                                                        }
                                                        res.send(response);
                                                        
                                                    }else{
                                                        response = {
                                                            "msg" : "La cuenta destino no existe"
                                                        }
                                                        res.status(404);
                                                        res.send(response);
                                                    }
                                                }
                                            }
                                        );
                                        
                                    }
                                    
                                }
                                else{
                                    response = {
                                       "msg" : "Acceso No Autorizado"
                                    }
                                    res.status(401);
                                    res.send(response);
                                }
                            }
                        }
                     );
                        

               }
        }

     }
);


function validartoken(tokenjwt){

  var payload=jwt.decode(tokenjwt,jwtClave);
  console.log("payload: " + payload);
  var respuesta;
  if(payload==null) {
     respuesta = {
       "email" : -1
     }
  } else {
    if(payload.exp <= moment().unix()) {
      respuesta = {
        "email" : -2,
      }
    }else {
        respuesta=payload;
      }

    }
    return(respuesta);
}

function generarNumeroCuenta() {
   var grupo1=getRandomNumber(0,99);
   var grupo2="9182";
   var grupo3=getRandomNumber(0,9999);
   var grupo4=getRandomNumber(0,9999);
   var grupo5=getRandomNumber(0,9999);
   var grupo6=getRandomNumber(0,9999);
   iban="ES"+grupo1+grupo2+grupo3+grupo4+grupo5+grupo6;
   return(iban);
}

function getRandomNumber(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
